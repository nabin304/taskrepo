﻿using System;
using Newtonsoft.Json;

namespace App.LoadBalance
{
    public class Vm
    {
        public Vm(string id, int ram)
        {
            Id = id;
            Ram = ram;
        }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        private int _ram;

        [JsonProperty(PropertyName = "ram")]
        public int Ram
        {
            get { return _ram; }
            set
            {
                if (value >= 1 && value <= 8192)
                    _ram = value;
                else
                {
                    throw new Exception("Ram must be in the range of 1-8192");
                }
            }
        }       
    }
}

