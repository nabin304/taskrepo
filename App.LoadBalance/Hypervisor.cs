﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace App.LoadBalance
{
    public class Hypervisor
    {
        public Hypervisor(string id, int maxram)
        {
            Id = id;
            Maxram = maxram;
        }

        [JsonIgnore]
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        private int _maxram;

        [JsonIgnore]
        [JsonProperty(PropertyName = "maxram")]
        public int Maxram
        {
            get { return _maxram; }
            set
            {
                if (value >= 1 && value <= 8192)
                    _maxram = value;
                else
                {
                    throw new Exception("Maxram must be in the range of 1-8192");
                }
            }
        }

        [JsonProperty(PropertyName = "vms")]
        public List<Vm> Vms { get; set; }
    }
}