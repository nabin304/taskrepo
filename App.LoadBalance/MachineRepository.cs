﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace App.LoadBalance
{
    public class MachineCreationLogic
    {
        private readonly LoadBalanceLogic _loadBalanceLogic;

        public MachineCreationLogic(LoadBalanceLogic loadBalanceLogic)
        {
            _loadBalanceLogic = loadBalanceLogic;            
        }

        /// <summary>
        /// Read  VMs from a json file
        /// </summary>
        /// <returns></returns>
        private List<Vm> ReadVirtualMachineFromJson()
        {
            var path = PathHelper.GetAbsolutePath(@"..\..\Data\vms.json");
            var deserializeVMs = JsonConvert.DeserializeObject<Dictionary<string, List<Vm>>>(File.ReadAllText(path));
            return deserializeVMs?.Values.FirstOrDefault();
        }

        /// <summary>
        /// Read Hypervisors from a json file
        /// </summary>
        /// <returns></returns>
        private  List<Hypervisor> ReadHypervisorsFromJson()
        {
            var path = PathHelper.GetAbsolutePath(@"..\..\Data\hypervisor.json");
            var deserializeHypervisors = JsonConvert.DeserializeObject<Dictionary<string, List<Hypervisor>>>(File.ReadAllText(path));
            return deserializeHypervisors?.Values.FirstOrDefault();
        }


        /// <summary>
        /// Create a load balanced hypervisors
        /// </summary>
        public void CreateOptimalHypervisorDistribution()
        {
            var optimalDistribution = GetOptimalDistribution();
            WriteHypervisorsToJson(optimalDistribution);
        }        

        private Dictionary<string, Hypervisor> GetOptimalDistribution()
        {
            var hypervisors = ReadHypervisorsFromJson();
            var vms = ReadVirtualMachineFromJson();
            return _loadBalanceLogic.CreateOptimalHypervisorDistribution(hypervisors, vms);
        }

        private void WriteHypervisorsToJson(Dictionary<string, Hypervisor> optimizedHypervisors)
        {
            var path = PathHelper.GetAbsolutePath(@"..\..\Data\result.json");
            using (StreamWriter file = File.CreateText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, optimizedHypervisors);
            }
        }
    }
}