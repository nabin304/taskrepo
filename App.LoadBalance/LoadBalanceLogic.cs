﻿using System.Collections.Generic;
using System.Linq;

namespace App.LoadBalance
{
    public class LoadBalanceLogic
    {
        private readonly Dictionary<Hypervisor, double> _hypervisorWeight = new Dictionary<Hypervisor, double>();

        public Dictionary<string, Hypervisor> CreateOptimalHypervisorDistribution(List<Hypervisor> hypervisors, List<Vm> vms)
        {
            foreach (var vm in vms)
            {
                _hypervisorWeight.Clear();

                foreach (var hypervisor in hypervisors)
                {
                    int availableRam = GetAvailableRam(hypervisor);

                    if (availableRam >= vm.Ram)
                    {
                        AssignVmToHypervisor(vm, hypervisor);
                        UpdateHypervisorWeights(hypervisors, hypervisor);
                    }
                }
                RemoveNonOptimalVmAssignment(hypervisors, vm);
            }
            var dictionary = hypervisors.ToDictionary(x => x.Id, y => y);
            return dictionary;
        }
       

        private int GetAvailableRam(Hypervisor hypervisor)
        {
            var sum = hypervisor.Vms?.Sum(r => r.Ram) ?? 0;
            var availableRam = hypervisor.Maxram - sum;
            return availableRam;
        }

        private void UpdateHypervisorWeights(List<Hypervisor> hypervisors, Hypervisor hypervisor)
        {            
            var currentLoadDistribution = GetCurrentLoadDistribution(hypervisors);
            var difference = GetMaximumWeightDifference(currentLoadDistribution);
            _hypervisorWeight.Add(hypervisor, difference);
        }

        private void RemoveNonOptimalVmAssignment(List<Hypervisor> hypervisors, Vm vm)
        {
            // Optimal Hypervisor is one, whose hypervisorWeight is least
            var optimalHypervisor = _hypervisorWeight.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
            foreach (var hypervisor in hypervisors)
            {
                if (hypervisor != optimalHypervisor)
                    hypervisor.Vms.Remove(vm);
            }
        }

        private void AssignVmToHypervisor(Vm vm, Hypervisor hypervisor)
        {
            if (hypervisor.Vms == null)
                hypervisor.Vms = new List<Vm>();
            hypervisor.Vms.Add(vm);
        }

        private double GetMaximumWeightDifference(Dictionary<Hypervisor, double> hp)
        {
            var min = hp.Min(v => v.Value);
            var max = hp.Max(v => v.Value);
            return max - min;
        }

        private Dictionary<Hypervisor, double> GetCurrentLoadDistribution(List<Hypervisor> hypervisors)
        {
            return hypervisors.ToDictionary(x => x, x =>
            {
                var sum = x.Vms?.Sum(y => y.Ram);
                if (sum != null)
                    return (double)sum / x.Maxram;
                return 0;
            });
        }
    }
}