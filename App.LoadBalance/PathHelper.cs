﻿using System;
using System.IO;

namespace App.LoadBalance
{
    public static class PathHelper
    {
        /// <summary>
        /// retuns a absoulte path
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetAbsolutePath(string filePath)
        {
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var combinedPath = Path.Combine(baseDirectory,filePath);
            return Path.GetFullPath(combinedPath);
        }
    }
}