﻿namespace App.LoadBalance
{
    class Program
    {
        static void Main(string[] args)
        {
            var loadBalanceLogic = new LoadBalanceLogic();
            var machineCreationLogic = new MachineCreationLogic(loadBalanceLogic);
            machineCreationLogic.CreateOptimalHypervisorDistribution();
        }
    }
}
